package application;

import entities.People;

public class Program {
    public static void main(String[] args) {

        People people = new People();
        People people2 = new People();

        people.setId(1);
        people.setName("Lucas Galvao");
        people.setFather("Undefined");
        people.setMother("Undefined");

        people2.setId(2);
        people2.setName("Lucas Galvao");
        people2.setFather("Undefined");
        people2.setMother("Undefined");

        if (people.equals(people2)) {
            System.out.println("People equals!");
            System.out.print(people + " ," + people2 + " ");
        } else {
            System.out.println("People != :)");
        }

    }
}
