package entities;

import java.util.Objects;

public class People {

    private Integer id;
    private String name;
    private String father;
    private String mother;

    public People() {
    }

    public People(Integer id, String name, String father, String mother) {
        this.id = id;
        this.name = name;
        this.father = father;
        this.mother = mother;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    @Override
    public String toString() {
        return " id " + id + "\n" +
                " name " + name + "\n" +
                " father " + father + "\n" +
                " mother " + mother + "\n";
    }

    @Override
    public boolean equals(Object objecttId) {
        if (this == objecttId) return true;
        if (objecttId == null || getClass() != objecttId.getClass()) return false;
        People people = (People) objecttId;
        return id.equals(people.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
